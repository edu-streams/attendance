﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Attendance_backend.Models;

namespace Attendance_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserDetailsController : ControllerBase
    {
        private readonly UserDetailsContext _context;

        public UserDetailsController(UserDetailsContext context)
        {
            _context = context;
        }

        // GET: api/UserDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDetails>>> GetStudentDetails()
        {
          if (_context.StudentDetails == null)
          {
              return NotFound();
          }
            return await _context.StudentDetails.ToListAsync();
        }

        // GET: api/UserDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDetails>> GetUserDetails(int id)
        {
          if (_context.StudentDetails == null)
          {
              return NotFound();
          }
            var userDetails = await _context.StudentDetails.FindAsync(id);

            if (userDetails == null)
            {
                return NotFound();
            }

            return userDetails;
        }

        // PUT: api/UserDetails/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserDetails(int id, UserDetails userDetails)
        {
            if (id != userDetails.id)
            {
                return BadRequest();
            }

            _context.Entry(userDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserDetails
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<UserDetails>> PostUserDetails(UserDetails userDetails)
        {
          if (_context.StudentDetails == null)
          {
              return Problem("Entity set 'UserDetailsContext.StudentDetails'  is null.");
          }
            _context.StudentDetails.Add(userDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserDetails", new { id = userDetails.id }, userDetails);
        }

        // DELETE: api/UserDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserDetails(int id)
        {
            if (_context.StudentDetails == null)
            {
                return NotFound();
            }
            var userDetails = await _context.StudentDetails.FindAsync(id);
            if (userDetails == null)
            {
                return NotFound();
            }

            _context.StudentDetails.Remove(userDetails);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserDetailsExists(int id)
        {
            return (_context.StudentDetails?.Any(e => e.id == id)).GetValueOrDefault();
        }
    }
}
