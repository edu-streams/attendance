﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Attendance_backend.Migrations
{
    /// <inheritdoc />
    public partial class FixedTypo : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "event_Id",
                table: "StudentDetails",
                newName: "event_id");

            migrationBuilder.RenameColumn(
                name: "iser_Id",
                table: "StudentDetails",
                newName: "user_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "event_id",
                table: "StudentDetails",
                newName: "event_Id");

            migrationBuilder.RenameColumn(
                name: "user_id",
                table: "StudentDetails",
                newName: "iser_Id");
        }
    }
}
