﻿using Microsoft.EntityFrameworkCore;

namespace Attendance_backend.Models
{
    public class UserDetailsContext : DbContext
    {
        public UserDetailsContext(DbContextOptions<UserDetailsContext> options) : base(options)
        {

        }
        public DbSet<UserDetails> StudentDetails { get; set; }
    }
}
