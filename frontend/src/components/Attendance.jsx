import { Box } from "@mui/material";

import StudentsList from "./StudentsList";
import SearchInput from "./SearchInput";

function Attendance({ presentStudents }) {

  return (
    <Box
      sx={{
        display: "block",
        width: "288px",
        minHeight: "300px",
        margin: "0 auto",
        paddingTop: "20px",
        paddingLeft: "10px",
        paddingRight: "10px",
        bgcolor: "#FAFAFA",
        boxShadow:
          "0px 1px 2px rgba(0, 0, 0, 0.06), 0px 1px 3px rgba(0, 0, 0, 0.1)",
      }}
    >
      <SearchInput students={presentStudents} />
      <StudentsList
        presentStudents={presentStudents}
      />
    </Box>
  );
}
export default Attendance;
