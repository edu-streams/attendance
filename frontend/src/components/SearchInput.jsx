import { useState } from "react";

import { InputAdornment, TextField, Typography } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

import StudentsItem from "./StudentItem";

function SearchInput({ students }) {
  const [searchValue, setSearchValue] = useState("");
  const [filteredStudents, setFilteredStudents] = useState([]);

  const handleSearchChange = (event) => {
    setSearchValue(event.target.value);
    findStudent(event.target.value);
  };

  const findStudent = (filter) => {
    {
      if (filter.length > 1) {
        const filtered = students.filter((student) => {
          return student.name.toLowerCase().includes(filter.trim().toLowerCase());
        });
        setFilteredStudents(filtered);
      } else {
        setFilteredStudents([]);
      }
    }
  };

    if (!students.length) {
    return null;
  }

  return (
    <>
      <TextField
        id="search-input"
        label="Пошук учасника"
        variant="outlined"
        size="small"
        color="secondary"
        sx={{
          height: "32px",
          minWidth: "248px",
          padding: "4px",
          marginBottom: "40px",
          fontSize: "12px",
          lineHeight: "18px",
        }}
        value={searchValue}
        onChange={handleSearchChange}
        InputLabelProps={{
          sx: {
            color: "#71717A",
          },
        }}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end" color="#A1A1AA">
              <SearchIcon fontSize="medium" edge="end" />
            </InputAdornment>
          ),
        }}
      />
      {searchValue && filteredStudents.length ? (
        <StudentsItem students={filteredStudents} search />
      ) : null}
      {searchValue.length > 1 && !filteredStudents.length && (
        <Typography
          color="#A1A1AA"
          fontSize="14px"
          lineHeight="18px"
          sx={{ textAlign: "center", mb:'15px' }}
        >
          На жаль, такого студента не знайдено
        </Typography>
      )}
    </>
  );
}

export default SearchInput;
