import { Stack, Typography, Badge } from "@mui/material";
import { styled } from "@mui/material/styles";

const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    backgroundColor: "#44b700",
    color: "#44b700",
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    position: "absolute",
    right: "15px",
    bottom: "15px",

    "&::after": {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      borderRadius: "50%",
      animation: "ripple 1.2s infinite ease-in-out",
      border: "1px solid currentColor",
      content: '""',
    },
  },
  "@keyframes ripple": {
    "0%": {
      transform: "scale(.8)",
      opacity: 1,
    },
    "100%": {
      transform: "scale(2.4)",
      opacity: 0,
    },
  },
}));

function StudentsItem({ students }) {
  if (!students.length) {
    return null;
  }

  return (
    <Stack>
      {students?.map((student) => (
        <Stack key={student.sub}>
          <Stack
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginLeft:'16px'
            }}
          >
            <StyledBadge
              overlap="circular"
              anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
              variant="dot"
            >
              <Stack
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  mr: "10px",
                  mb: "10px",
                  width: "32px",
                  height: "32px",
                  borderRadius: "50%",
                  backgroundImage: `url(${student.picture})`,
                  backgroundSize: "contain",
                  color: "black",
                }}
              />
            </StyledBadge>
            <Stack>
              <Typography
                color="#18181B"
                fontSize="14px"
                lineHeight="18px"           
              >
                {student.name}
              </Typography>
            </Stack>
          </Stack>
        </Stack>
      ))}
    </Stack>
  );
}
export default StudentsItem;
