import { AccordionDetails, Typography } from "@mui/material";

import StudentsItem from "./StudentItem";

function StudentsList({ presentStudents }) {
  if (!presentStudents.length) {
    return null;
  }
  return (
    <>
      <Typography sx={{ mt: "7px", mb:'15px', ml:'5px' }}>
        Присутні на зустрічі ({presentStudents.length})
      </Typography>
      <StudentsItem students={presentStudents} />
    </>
  );
}
export default StudentsList;
