import Typography from "@mui/material/Typography";
import { useEffect } from "react";
import { socket } from "../../services/socket";
import * as jose from "jose";

const EventPageBlock = () => {
  useEffect(() => {
    const token = localStorage.getItem("logto:asnhh0px905tmbmdwlqm1:idToken");
    if (token) {
      const claims = jose.decodeJwt(token);
      socket.emit("user", claims);
    }
  }, []);

  return (
    <>
      <Typography>Event page block</Typography>
    </>
  );
};
export default EventPageBlock;
