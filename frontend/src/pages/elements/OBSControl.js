import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import Attendance from "../../components/Attendance";

const OBSControl = () => {
  const [isDisplay, setIsDisplay] = useState(true);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const resultOptions = { isDisplay };
    socket.emit("control", resultOptions);
  }, [isDisplay]);

  useEffect(() => {
    socket.on("user", (data) => {
      console.log("data", data);
      setUsers((prevstate) => {
        const duplicate = prevstate.find((user) => user.sub === data.sub);
        if (duplicate) {
          return prevstate;
        } else {
          return [...prevstate, data];
        }
      });
    });
  }, []);

  return (
    <>
      <Attendance presentStudents={users} />
    </>
  );
};
export default OBSControl;
