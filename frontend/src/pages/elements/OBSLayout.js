import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";

const OBSLayout = () => {
  const [isDisplay, setIsDisplay] = useState(true);

  useEffect(() => {
    socket.on("control", (control) => {
      setIsDisplay(control);
      console.log(control);
    });
  }, []);

  return (
    <>
      <Typography>OBS Layout</Typography>
      <Box display={isDisplay}></Box>
    </>
  );
};
export default OBSLayout;
