import { red } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

// A custom theme for this app
const theme = createTheme({
  palette: {
    primary: {
      main: "#556cd6",
    },
    secondary: {
      main: "#71717A",
    },
    error: {
      main: red.A400,
    },
    textField: {
      color: "#71717A",
    },
    icon: { color: "#A1A1AA" },
  },
});

export default theme;
